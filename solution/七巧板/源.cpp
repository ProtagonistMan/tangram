#include<iostream>
#include<graphics.h>
#include<vector>
#include<math.h>
#include<list>
#include<time.h>
#include<conio.h>
#include"Line.h"
#define abs(T) ((T)>0?(T):(-(T)))
#define SIDELENGTH 100
#define BUTTON 60
#define INBUTTON1(X,Y) (((X)>0)&&((X)<SIDELENGTH*5/2)&&((Y)>SIDELENGTH*5)&&((Y)<SIDELENGTH*5+BUTTON))
#define INBUTTON2(X,Y) (((X)>SIDELENGTH*5/2)&&((X)<SIDELENGTH*5)&&((Y)>SIDELENGTH*5)&&((Y)<SIDELENGTH*5+BUTTON))
using namespace std;
int nextId = 0;
template<class T,class Y>
class KeyValue
{
public:
	T Value;
	Y Key;
	KeyValue(T val, Y key)
	{
		Value = val;
		Key = key;
	}
};
vector<KeyValue<COLORREF,int>> PUBLICORIGINAL[SIDELENGTH * 5][SIDELENGTH * 5];
class Graph
{
	int Id;
public:
	Graph()
	{
		Id = nextId;
		nextId++;
	}
	virtual void draw()
	{
	}
	virtual void fill() {}
	virtual void setMyColor(COLORREF val) {}
	virtual bool ifInside(Point num) { return true; }
	vector<COLORREF> original;
	virtual void removeMe() {}
	virtual void operator+(Point num) {}
	virtual void spin(Point original, Point finalPoint) {}
	virtual void spin45Degree(){}
	virtual void symmetricalSpin() {}
	int getId() { return Id; }
};
class Triangle :public Graph
{
	Point A, B, C;
	Line AB;
	Line BC;
	Line CA;
	Point centerPoint;
	COLORREF myColor;
public:
	Triangle(Point A, Point B, Point C)
	{
		this->A = A;
		this->B = B;
		this->C = C;
		AB = Line(A, B);
		BC = Line(B, C);
		CA = Line(C, A);
		myColor = BLACK;
		//求三角形外接圆的圆心，也许内接圆的圆心更适合
		// 根据三角形面积求内接圆半径，找到距离边长为内接圆半径的直线两条，求交点就是三角形内接圆圆心
		//根据直线AB，AC求两直线垂直平分线交点
		// 根据(x1,y1)、(x2,y2)两点得到直线方程：(y2-y1)x+(x1-x2)y+x2y1-x1y2=0
		//若Ax+By+C=0为直线方程，则Bx-Ay+D=0(D为任意值)为直线垂直线方程，因为斜率乘积为-1
		//根据线段中点((x1+x2)/2,(y1+y2)/2)得到D值，D=Ay-Bx，在新的方程中为C=-By-Ax
		//联立A1x+B1y+C1=0和A2x+B2y+C2=0得
		//x=(B2C1-B1C2)/(A2B1-A1B2)         y=(A1C2-A2C1)/(A2B1-A1B2)
		//说实话我要是线代学好压根不用自己化，直接带公式就行了
		double A1 = A.x - B.x;
		double B1 = A.y - B.y;
		double C1 = (B.y - A.y) * (B.y + A.y) / 2 - (A.x - B.x) * (A.x + B.x) / 2;
		double A2 = A.x - C.x;
		double B2 = A.y - C.y;
		double C2 = (C.y - A.y) * (C.y + A.y) / 2 - (A.x - C.x) * (A.x + C.x) / 2;
		double x = (B2 * C1 - B1 * C2) / (A2 * B1 - A1 * B2);
		double y = (A2 * C1 - A1 * C2) / (A1 * B2 - A2 * B1);
		centerPoint = Point(x, y);
	}
	void draw()
	{
		for (Point i : AB.getLinePoint())
		{
			putpixel((int)(i.x + 0.5), (int)(i.y + 0.5), getlinecolor());
		}
		for (Point i : BC.getLinePoint())
		{
			putpixel((int)(i.x + 0.5), (int)(i.y + 0.5), getlinecolor());
		}
		for (Point i : CA.getLinePoint())
		{
			putpixel((int)(i.x + 0.5), (int)(i.y + 0.5), getlinecolor());
		}
	}
	vector<Point>getTrianglePoint()
	{
		vector<Point> result;
		//double rate_AB = 1/  AB.getLineLength();
		//double rate_BC = 1 / BC.getLineLength();
		//double rate_CA = 1 / CA.getLineLength();
		////double finalRate = min(min(rate_AB, rate_BC), rate_CA);
		//double finalRate = min(rate_BC, rate_CA);
		
		double left = min(min(A.x, B.x), C.x);
		double right = max(max(A.x, B.x), C.x);
		double top = max(max(A.y, B.y), C.y);
		double buttom = min(min(A.y, B.y), C.y);
		for (double i = (int)buttom-1;i <= (int)top+1;i += 1)
		{
			for (double j = (int)left-1;j <= (int)right+1;j += 1)
			{
				if (ifInTriangle(Point(j, i)))
				{
					result.push_back(Point(j, i));
				}
			}
		}
		return result;
	}
	void fill()
	{
		vector<Point> num = getTrianglePoint();
		for (Point i : num)
		{
			if (i.x >= SIDELENGTH * 5 || i.x < 0 || i.y >= SIDELENGTH * 5 || i.y < 0)continue;
			putpixel((int)(i.x + 0.5), (int)(i.y + 0.5), myColor);
			PUBLICORIGINAL[(int)(i.y + 0.5)][(int)(i.x + 0.5)].push_back(KeyValue<COLORREF,int>(myColor,this->getId()));
		}
	}
	void setMyColor(COLORREF val)//COLORREF是整型，可以直接赋值
	{
		myColor = val;
	}
	double getSquare()
	{
		return AB.getPointToLineDistance(C) * AB.getLineLength() / 2;
	}
	bool ifInTriangle(Point num)
	{
		Line AN = Line(A, num);
		Point A_N = AN.getIntersectionPoint(BC);
		if (A_N.getDistance(A) - A.getDistance(num)<-0.5)return false;
		Line BN = Line(B, num);
		Point B_N = BN.getIntersectionPoint(CA);
		if (B_N.getDistance(B) - B.getDistance(num)<-0.5)return false;
		Line CN = Line(C, num);
		Point C_N = CN.getIntersectionPoint(AB);
		if (C_N.getDistance(C) - C.getDistance(num)<-0.5)return false;
		return true;
	}
	bool ifInside(Point num)
	{
		if (ifInTriangle(num))return true;
		return false;
	}
	void removeMe()
	{
		vector<Point> num = getTrianglePoint();
		for (Point i : num)
		{
			if (i.x >= SIDELENGTH * 5 || i.x < 0 || i.y >= SIDELENGTH * 5 || i.y < 0)continue;
			vector<KeyValue<COLORREF, int>>::iterator ite = PUBLICORIGINAL[(int)(i.y + 0.5)][(int)(i.x + 0.5)].begin();
			for (int j = 1;j < PUBLICORIGINAL[(int)(i.y + 0.5)][(int)(i.x + 0.5)].size();j++)
			{
				if (PUBLICORIGINAL[(int)(i.y + 0.5)][(int)(i.x + 0.5)][j].Key == this->getId())
				{
					PUBLICORIGINAL[(int)(i.y + 0.5)][(int)(i.x + 0.5)].erase(ite + j);
					break;
				}
			}
			putpixel((int)(i.x + 0.5), (int)(i.y + 0.5),
				PUBLICORIGINAL[(int)(i.y + 0.5)][(int)(i.x + 0.5)].back().Value);
		}
	}
	void spin45Degree()
	{
		double SinI = sqrt(2) / 2;
		double CosI = sqrt(2) / 2;

		double SinA = (A.y - centerPoint.y) / A.getDistance(centerPoint);
		double CosA = (A.x - centerPoint.x) / A.getDistance(centerPoint);
		double SinAF = SinI * CosA + SinA * CosI;
		double CosAF = CosI * CosA - SinI * SinA;
		A = Point(A.getDistance(centerPoint) * CosAF + centerPoint.x, A.getDistance(centerPoint) * SinAF + centerPoint.y);

		double SinB = (B.y - centerPoint.y) / B.getDistance(centerPoint);
		double CosB = (B.x - centerPoint.x) / B.getDistance(centerPoint);
		double SinBF = SinI * CosB + SinB * CosI;
		double CosBF = CosI * CosB - SinI * SinB;
		B = Point(B.getDistance(centerPoint) * CosBF + centerPoint.x, B.getDistance(centerPoint) * SinBF + centerPoint.y);

		double SinC = (C.y - centerPoint.y) / C.getDistance(centerPoint);
		double CosC = (C.x - centerPoint.x) / C.getDistance(centerPoint);
		double SinCF = SinI * CosC + SinC * CosI;
		double CosCF = CosI * CosC - SinI * SinC;
		C = Point(C.getDistance(centerPoint) * CosCF + centerPoint.x, C.getDistance(centerPoint) * SinCF + centerPoint.y);

		AB.setPointA(A);
		AB.setPointB(B);

		BC.setPointA(B);
		BC.setPointB(C);

		CA.setPointA(C);
		CA.setPointB(A);
	}
	void spin(Point original, Point finalPoint)
	{
		//用三角函数来解
		if (original == centerPoint || finalPoint == centerPoint)return;
		double SinO = (original.y - centerPoint.y) / original.getDistance(centerPoint);
		double CosO = (original.x - centerPoint.x) / original.getDistance(centerPoint);
		double SinF = (finalPoint.y - centerPoint.y) / finalPoint.getDistance(centerPoint);
		double CosF = (finalPoint.x - centerPoint.x) / finalPoint.getDistance(centerPoint);
		double SinI = SinF * CosO - SinO * CosF;
		double CosI = CosF * CosO + SinF * SinO;
		double SinA = (A.y - centerPoint.y) / A.getDistance(centerPoint);
		double CosA = (A.x - centerPoint.x) / A.getDistance(centerPoint);
		double SinAF = SinI * CosA + SinA * CosI;
		double CosAF = CosI * CosA - SinI * SinA;
		A = Point(A.getDistance(centerPoint) * CosAF + centerPoint.x, A.getDistance(centerPoint) * SinAF + centerPoint.y);

		double SinB = (B.y - centerPoint.y) / B.getDistance(centerPoint);
		double CosB = (B.x - centerPoint.x) / B.getDistance(centerPoint);
		double SinBF = SinI * CosB + SinB * CosI;
		double CosBF = CosI * CosB - SinI * SinB;
		B = Point(B.getDistance(centerPoint) * CosBF + centerPoint.x, B.getDistance(centerPoint) * SinBF + centerPoint.y);

		double SinC = (C.y - centerPoint.y) / C.getDistance(centerPoint);
		double CosC = (C.x - centerPoint.x) / C.getDistance(centerPoint);
		double SinCF = SinI * CosC + SinC * CosI;
		double CosCF = CosI * CosC - SinI * SinC;
		C = Point(C.getDistance(centerPoint) * CosCF + centerPoint.x, C.getDistance(centerPoint) * SinCF + centerPoint.y);

		AB.setPointA(A);
		AB.setPointB(B);

		BC.setPointA(B);
		BC.setPointB(C);

		CA.setPointA(C);
		CA.setPointB(A);
	}
	void operator+(Point num)
	{
		A + num;
		B + num;
		C + num;
		AB + num;
		BC + num;
		CA + num;
		centerPoint + num;
	}
};
class Quadrangle :public Graph
{
	Point A, B, C, D;
	Line AB, BC, CD, DA;
	Point centerPoint;
	COLORREF myColor;
	vector<Point> overlap;
	vector<COLORREF> overlapColor;
public:
	Quadrangle(Point a, Point b, Point c, Point d)
	{
		A = a;
		B = b;
		C = c;
		D = d;
		AB = Line(a, b);
		BC = Line(b, c);
		CD = Line(c, d);
		DA = Line(d, a);
		myColor = 0;
		//对于四边形来说，对角线的交点就是中心点
		centerPoint = Line(a, c).getIntersectionPoint(Line(b, d));
	}
	void draw()
	{
		for (Point i : AB.getLinePoint())
		{
			putpixel((int)(i.x + 0.5), (int)(i.y + 0.5), getlinecolor());
		}
		for (Point i : BC.getLinePoint())
		{
			putpixel((int)(i.x + 0.5), (int)(i.y + 0.5), getlinecolor());
		}
		for (Point i : CD.getLinePoint())
		{
			putpixel((int)(i.x + 0.5), (int)(i.y + 0.5), getlinecolor());
		}
		for (Point i : DA.getLinePoint())
		{
			putpixel((int)(i.x + 0.5), (int)(i.y + 0.5), getlinecolor());
		}
	}
	double getSquare()
	{
		return (Line(A, C).getPointToLineDistance(B) + Line(A, C).getPointToLineDistance(D)) * Line(A, C).getLineLength() / 2;
	}
	bool ifInQuadrangle(Point num)
	{
		if (Triangle(A, B, C).ifInTriangle(num))return true;
		if (Triangle(A, C, D).ifInTriangle(num))return true;

		/*if (AB.getPointToLineDistance(num) * AB.getLineLength() / 2 +
			BC.getPointToLineDistance(num) * BC.getLineLength() / 2 +
			CD.getPointToLineDistance(num) * CD.getLineLength() / 2 +
			DA.getPointToLineDistance(num)*DA.getLineLength()/2> this->getSquare())return false;*/
		return false;
	}
	vector<Point>getQuadranglePoint()
	{
		vector<Point> result;
		double left = min(min(A.x, B.x), min(C.x, D.x));
		double right = max(max(A.x, B.x), max(C.x, D.x));
		double buttom = min(min(A.y, B.y), min(C.y, D.y));
		double top = max(max(A.y, B.y), max(C.y, D.y));
		for (double i = (int)buttom-1;i <= (int)top+1;i += 1)
		{
			for (double j = (int)left-1;j <= (int)right+1;j += 1)
			{
				if (ifInQuadrangle(Point(j, i)))
				{
					result.push_back(Point(j, i));
				}
			}
		}
		return result;
	}
	void symmetricalSpin()
	{
		double x = centerPoint.x;
		A.x = x + x - A.x;
		B.x = x + x - B.x;
		C.x = x + x - C.x;
		D.x = x + x - D.x;
	}

	void fill()
	{
		vector<Point>num = getQuadranglePoint();
		for (Point i : num)
		{
			//putpixel((int)(i.x + 0.5), (int)(i.y + 0.5), BLUE);
			if (i.x >= SIDELENGTH * 5 || i.x < 0 || i.y >= SIDELENGTH * 5 || i.y < 0)continue;
			putpixel(i.x, i.y, myColor);
			PUBLICORIGINAL[(int)(i.y + 0.5)][(int)(i.x + 0.5)].push_back(KeyValue<COLORREF, int>(myColor, this->getId()));
		}
	}
	void setMyColor(COLORREF val)
	{
		myColor = val;
	}
	bool ifInside(Point num)
	{
		if (ifInQuadrangle(num))return true;
		return false;
	}
	void removeMe()
	{
		vector<Point> num = getQuadranglePoint();
		for (Point i : num)
		{
			if (i.x >= SIDELENGTH * 5 || i.x < 0 || i.y >= SIDELENGTH * 5 || i.y < 0)continue;
			vector<KeyValue<COLORREF, int>>::iterator ite = PUBLICORIGINAL[(int)(i.y + 0.5)][(int)(i.x + 0.5)].begin();
			for (int j = 1;j < PUBLICORIGINAL[(int)(i.y + 0.5)][(int)(i.x + 0.5)].size();j++)
			{
				if (PUBLICORIGINAL[(int)(i.y + 0.5)][(int)(i.x + 0.5)][j].Key == this->getId())
				{
					PUBLICORIGINAL[(int)(i.y + 0.5)][(int)(i.x + 0.5)].erase(ite + j);
					break;
				}
			}
			putpixel((int)(i.x + 0.5), (int)(i.y + 0.5),
				PUBLICORIGINAL[(int)(i.y + 0.5)][(int)(i.x + 0.5)].back().Value);
		}
	}
	void spin45Degree()
	{
		double SinI = sqrt(2) / 2;
		double CosI = sqrt(2) / 2;

		double SinA = (A.y - centerPoint.y) / A.getDistance(centerPoint);
		double CosA = (A.x - centerPoint.x) / A.getDistance(centerPoint);
		double SinAF = SinI * CosA + SinA * CosI;
		double CosAF = CosI * CosA - SinI * SinA;
		A = Point(A.getDistance(centerPoint) * CosAF + centerPoint.x, A.getDistance(centerPoint) * SinAF + centerPoint.y);

		double SinB = (B.y - centerPoint.y) / B.getDistance(centerPoint);
		double CosB = (B.x - centerPoint.x) / B.getDistance(centerPoint);
		double SinBF = SinI * CosB + SinB * CosI;
		double CosBF = CosI * CosB - SinI * SinB;
		B = Point(B.getDistance(centerPoint) * CosBF + centerPoint.x, B.getDistance(centerPoint) * SinBF + centerPoint.y);

		double SinC = (C.y - centerPoint.y) / C.getDistance(centerPoint);
		double CosC = (C.x - centerPoint.x) / C.getDistance(centerPoint);
		double SinCF = SinI * CosC + SinC * CosI;
		double CosCF = CosI * CosC - SinI * SinC;
		C = Point(C.getDistance(centerPoint) * CosCF + centerPoint.x, C.getDistance(centerPoint) * SinCF + centerPoint.y);

		double SinD = (D.y - centerPoint.y) / D.getDistance(centerPoint);
		double CosD = (D.x - centerPoint.x) / D.getDistance(centerPoint);
		double SinDF = SinI * CosD + SinD * CosI;
		double CosDF = CosI * CosD - SinI * SinD;
		D = Point(D.getDistance(centerPoint) * CosDF + centerPoint.x, D.getDistance(centerPoint) * SinDF + centerPoint.y);


		AB.setPointA(A);
		AB.setPointB(B);

		BC.setPointA(B);
		BC.setPointB(C);

		CD.setPointA(C);
		CD.setPointB(D);

		DA.setPointA(D);
		DA.setPointB(A);
	}
	void spin(Point original, Point finalPoint)
	{
		if (original == centerPoint || finalPoint == centerPoint)return;

		double SinO = (original.y - centerPoint.y) / original.getDistance(centerPoint);
		double CosO = (original.x - centerPoint.x) / original.getDistance(centerPoint);
		double SinF = (finalPoint.y - centerPoint.y) / finalPoint.getDistance(centerPoint);
		double CosF = (finalPoint.x - centerPoint.x) / finalPoint.getDistance(centerPoint);
		double SinI = SinF * CosO - SinO * CosF;
		double CosI = CosF * CosO + SinF * SinO;
		double SinA = (A.y - centerPoint.y) / A.getDistance(centerPoint);
		double CosA = (A.x - centerPoint.x) / A.getDistance(centerPoint);
		double SinAF = SinI * CosA + SinA * CosI;
		double CosAF = CosI * CosA - SinI * SinA;
		A = Point(A.getDistance(centerPoint) * CosAF + centerPoint.x, A.getDistance(centerPoint) * SinAF + centerPoint.y);

		double SinB = (B.y - centerPoint.y) / B.getDistance(centerPoint);
		double CosB = (B.x - centerPoint.x) / B.getDistance(centerPoint);
		double SinBF = SinI * CosB + SinB * CosI;
		double CosBF = CosI * CosB - SinI * SinB;
		B = Point(B.getDistance(centerPoint) * CosBF + centerPoint.x, B.getDistance(centerPoint) * SinBF + centerPoint.y);

		double SinC = (C.y - centerPoint.y) / C.getDistance(centerPoint);
		double CosC = (C.x - centerPoint.x) / C.getDistance(centerPoint);
		double SinCF = SinI * CosC + SinC * CosI;
		double CosCF = CosI * CosC - SinI * SinC;
		C = Point(C.getDistance(centerPoint) * CosCF + centerPoint.x, C.getDistance(centerPoint) * SinCF + centerPoint.y);

		double SinD = (D.y - centerPoint.y) / D.getDistance(centerPoint);
		double CosD = (D.x - centerPoint.x) / D.getDistance(centerPoint);
		double SinDF = SinI * CosD + SinD * CosI;
		double CosDF = CosI * CosD - SinI * SinD;
		D = Point(D.getDistance(centerPoint) * CosDF + centerPoint.x, D.getDistance(centerPoint) * SinDF + centerPoint.y);


		AB.setPointA(A);
		AB.setPointB(B);

		BC.setPointA(B);
		BC.setPointB(C);

		CD.setPointA(C);
		CD.setPointB(D);

		DA.setPointA(D);
		DA.setPointB(A);
	}
	void operator+(Point num)
	{
		A + num;
		B + num;
		C + num;
		D + num;
		AB + num;
		BC + num;
		CD + num;
		DA + num;
		centerPoint + num;
	}
};

list<Graph*> Create()
{
	list<Graph*> result;
	double size = SIDELENGTH;
	double SIDE = 5 * size;
	Graph* bigTri1 = new Triangle(Point(SIDE / 2, SIDE / 2),
		Point(SIDE / 2 - size / 2, SIDE / 2 - size / 2), Point(SIDE / 2 - size / 2, SIDE / 2 + size / 2));
	bigTri1->setMyColor(RGB(242, 60, 72));
	result.push_back(bigTri1);
	Graph* bigTri2 = new Triangle(Point(SIDE / 2, SIDE / 2),
		Point(SIDE / 2 - size / 2, SIDE / 2 - size / 2), Point(SIDE / 2 + size / 2, SIDE / 2 - size / 2));
	bigTri2->setMyColor(RGB(201, 147, 113));
	result.push_back(bigTri2);
	Graph* smaTri1 = new Triangle(Point(SIDE / 2, SIDE / 2),
		Point(SIDE / 2 - size / 4, SIDE / 2 + size / 4), Point(SIDE / 2 + size / 4, SIDE / 2 + size / 4));
	smaTri1->setMyColor(RGB(249, 173, 38));
	result.push_back(smaTri1);
	Graph* smaTri2 = new Triangle(Point(SIDE / 2 + size / 4, SIDE / 2 - size / 4),
		Point(SIDE / 2 + size / 2, SIDE / 2 - size / 2), Point(SIDE / 2 + size / 2, SIDE / 2));
	smaTri2->setMyColor(RGB(249, 116, 105));
	result.push_back(smaTri2);
	Graph* midTri = new Triangle(Point(SIDE / 2 + size / 2, SIDE / 2 + size / 2),
		Point(SIDE / 2, SIDE / 2 + size / 2), Point(SIDE / 2 + size / 2, SIDE / 2));
	midTri->setMyColor(RGB(90, 181, 210));
	result.push_back(midTri);
	Graph* square = new Quadrangle(Point(SIDE / 2, SIDE / 2), Point(SIDE / 2 + size / 4, SIDE / 2 - size / 4),
		Point(SIDE / 2 + size / 2, SIDE / 2), Point(SIDE / 2 + size / 4, SIDE / 2 + size / 4));
	square->setMyColor(RGB(235, 210, 48));
	result.push_back(square);
	Graph* quare = new Quadrangle(Point(SIDE / 2 - size / 4, SIDE / 2 + size / 4), Point(SIDE / 2 + size / 4, SIDE / 2 + size / 4),
		Point(SIDE / 2, SIDE / 2 + size / 2), Point(SIDE / 2 - size / 2, SIDE / 2 + size / 2));
	quare->setMyColor(RGB(169, 198, 70));
	result.push_back(quare);
	return result;
}
void addNewTengram(list<Graph*>& result)
{
	//list<Graph*> result;
	double size = SIDELENGTH;
	double SIDE = 5 * size;
	Graph* bigTri1 = new Triangle(Point(SIDE / 2, SIDE / 2),
		Point(SIDE / 2 - size / 2, SIDE / 2 - size / 2), Point(SIDE / 2 - size / 2, SIDE / 2 + size / 2));
	bigTri1->setMyColor(RGB(242, 60, 72));
	bigTri1->fill();
	result.push_back(bigTri1);
	Graph* bigTri2 = new Triangle(Point(SIDE / 2, SIDE / 2),
		Point(SIDE / 2 - size / 2, SIDE / 2 - size / 2), Point(SIDE / 2 + size / 2, SIDE / 2 - size / 2));
	bigTri2->setMyColor(RGB(201, 147, 113));
	bigTri2->fill();
	result.push_back(bigTri2);
	Graph* smaTri1 = new Triangle(Point(SIDE / 2, SIDE / 2),
		Point(SIDE / 2 - size / 4, SIDE / 2 + size / 4), Point(SIDE / 2 + size / 4, SIDE / 2 + size / 4));
	smaTri1->setMyColor(RGB(249, 173, 38));
	smaTri1->fill();
	result.push_back(smaTri1);
	Graph* smaTri2 = new Triangle(Point(SIDE / 2 + size / 4, SIDE / 2 - size / 4),
		Point(SIDE / 2 + size / 2, SIDE / 2 - size / 2), Point(SIDE / 2 + size / 2, SIDE / 2));
	smaTri2->setMyColor(RGB(249, 116, 105));
	smaTri2->fill();
	result.push_back(smaTri2);
	Graph* midTri = new Triangle(Point(SIDE / 2 + size / 2, SIDE / 2 + size / 2),
		Point(SIDE / 2, SIDE / 2 + size / 2), Point(SIDE / 2 + size / 2, SIDE / 2));
	midTri->setMyColor(RGB(90, 181, 210));
	midTri->fill();
	result.push_back(midTri);
	Graph* square = new Quadrangle(Point(SIDE / 2, SIDE / 2), Point(SIDE / 2 + size / 4, SIDE / 2 - size / 4),
		Point(SIDE / 2 + size / 2, SIDE / 2), Point(SIDE / 2 + size / 4, SIDE / 2 + size / 4));
	square->setMyColor(RGB(235, 210, 48));
	square->fill();
	result.push_back(square);
	Graph* quare = new Quadrangle(Point(SIDE / 2 - size / 4, SIDE / 2 + size / 4), Point(SIDE / 2 + size / 4, SIDE / 2 + size / 4),
		Point(SIDE / 2, SIDE / 2 + size / 2), Point(SIDE / 2 - size / 2, SIDE / 2 + size / 2));
	quare->setMyColor(RGB(169, 198, 70));
	quare->fill();
	result.push_back(quare);
	//return result;
}
void init(list<Graph*> head)
{
	for (Graph* i : head)
	{
		i->fill();
	}
	setlinecolor(BLACK);
	line(0, SIDELENGTH * 5, SIDELENGTH * 5, SIDELENGTH * 5);
	line(SIDELENGTH * 5 / 2, SIDELENGTH * 5, SIDELENGTH * 5 / 2, SIDELENGTH * 5 + BUTTON);
	settextcolor(BLACK);
	outtextxy(20, SIDELENGTH * 5 + 20, "添加一套七巧板");
	outtextxy(SIDELENGTH * 5 / 2 + 20, SIDELENGTH * 5 + 20, "退出游戏");
}
int main()
{
	initgraph(SIDELENGTH * 5, SIDELENGTH * 5+BUTTON);
	setbkcolor(WHITE);
	setlinecolor(BLACK);
	setfillcolor(YELLOW);
	cleardevice();
	for (int i = 0;i < SIDELENGTH * 5;i++)
	{
		for (int j = 0;j < SIDELENGTH * 5;j++)
		{
			PUBLICORIGINAL[i][j].push_back(KeyValue<COLORREF,int>(getpixel(j, i),-1));
		}
	}
	clock_t now;
	list<Graph*> head = Create();
	init(head);
	MOUSEMSG msg;
	bool press = false;
	bool effect = false;
	bool effectR = false;
	bool effectB1 = false;
	bool effectB2 = false;
	Graph* operatObject = nullptr;
	int originalx = 0, originaly = 0;

	while (true)
	{
		if (MouseHit())
		{
			PeekMouseMsg(&msg);
			if (msg.mkLButton && !press)
			{
				press = true;
				for (Graph* i : head)
				{
					if (i->ifInside(Point(msg.x, msg.y)))
					{
						effect = true;
						operatObject = i;
						originalx = msg.x;
						originaly = msg.y;
						now = clock();
					}
				}
				if (INBUTTON1(msg.x, msg.y))effectB1 = true;
				else if (INBUTTON2(msg.x, msg.y))effectB2 = true;
			}
			else if (msg.mkLButton && press)
			{
				if (effect && (originalx != msg.x || originaly != msg.y) && (clock() - now) > CLOCKS_PER_SEC / 10)
				{
					if (_kbhit() && _getch() == ' ')
					{
						operatObject->removeMe();
						operatObject->symmetricalSpin();
						operatObject->fill();
					}
					operatObject->removeMe();
					*operatObject + Point(msg.x - originalx, msg.y - originaly);
					originalx = msg.x;
					originaly = msg.y;
					operatObject->fill();
					now = clock();
				}
			}
			else if (msg.mkRButton && !press)
			{
				press = true;
				for (Graph* i : head)
				{
					if (i->ifInside(Point(msg.x, msg.y)))
					{
						effectR = true;
						operatObject = i;
						originalx = msg.x;
						originaly = msg.y;
						now = clock();
					}
				}
			}
			else if (effectR && !msg.mkRButton&&press)
			{
				press = false;
				effectR = false;
				if (operatObject->ifInside(Point(msg.x, msg.y)))
				{
					operatObject->removeMe();
					operatObject->spin45Degree();
					operatObject->fill();
				}
			}
			else if (!msg.mkRButton && !msg.mkLButton && press)
			{
				press = false;
				effect = false;
				effectR = false;
				if(effectB1)addNewTengram(head);
				if (effectB2)break;
				effectB1 = false;
				effectB2 = false;
			}
		}
	}
	return 0;
}