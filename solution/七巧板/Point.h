#pragma once
#include<math.h>
class Point
{
public:
	double x;
	double y;
public:
	Point()
	{
		x = 0;
		y = 0;
	}
	Point(double xx, double yy)
	{
		x = xx;
		y = yy;
	}
	double getDistance(Point num)
	{
		return sqrt((this->x - num.x) * (this->x - num.x) + (this->y - num.y) * (this->y - num.y));
	}
	void operator=(Point num)
	{
		this->x = num.x;
		this->y = num.y;
	}
	void operator+(Point num)
	{
		this->x += num.x;
		this->y += num.y;
	}
	bool operator==(Point num)
	{
		if (this->x == num.x && this->y == num.y)return true;
		return false;
	}
};