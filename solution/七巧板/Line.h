#pragma once
#include"Point.h"
#include<vector>
using std::vector;
class Line
{
	Point A;
	Point B;
public:
	Line()
	{

	}
	Line(Point a, Point b)
	{
		A = a;
		B = b;
	}
	Line(double A_equation, double B_equation, double C_equation)//一般式获得直线
	{
		if (A_equation == 0 && B_equation == 0)return;
		if (A_equation == 0)
		{
			A = Point(B_equation, -C_equation / B_equation);
			B = Point(0, -C_equation / B_equation);
		}
		else if (B_equation == 0)
		{
			A = Point(-C_equation / A_equation, A_equation);
			B = Point(-C_equation / A_equation, 0);
		}
		else
		{
			A = Point(B_equation, (-C_equation - A_equation * B_equation) / B_equation);
			B = Point((-C_equation - A_equation * B_equation) / A_equation, A_equation);
		}
	}
	vector<Point> getLinePoint(bool endpoint = true)
	{
		//根据两点式(y-y1)/(x-x1)=(y-y2)/(x-x2)
		//y=(y1-y2)/(x1-x2)*x+(x1*y2-x2*y1)/(x1-x2)
		//x=(x1-x2)/(y1-y2)*y+(y1*x2-y2*x1)/(y1-y2)
		vector<Point> result;
		if (A.x != B.x)
		{
			//整数优先，两端再是小数
			double left = min(A.x, B.x);
			double right = max(A.x, B.x);
			double y1 = A.y;
			double y2 = B.y;
			double x1 = A.x;
			double x2 = B.x;
			result.push_back(Point(left, (y1 - y2) / (x1 - x2) * left + (x1 * y2 - x2 * y1) / (x1 - x2)));
			for (double i = (int)left + 1;i < right;i += 1)
			{
				double x, y;
				x = i;
				y = (y1 - y2) / (x1 - x2) * x + (x1 * y2 - x2 * y1) / (x1 - x2);
				result.push_back(Point(x, y));
				double nextY = y;
				if (x + 1 < right)nextY = (y1 - y2) / (x1 - x2) * (x + 1) + (x1 * y2 - x2 * y1) / (x1 - x2);
				else nextY = (y1 - y2) / (x1 - x2) * right + (x1 * y2 - x2 * y1) / (x1 - x2);
				if (abs(nextY - y) > 1)
				{
					//double down = min(y, nextY);
					//double up = max(y, nextY);
					if (y > nextY)
					{
						for (double j = y - 1;j > nextY;j -= 1)
						{
							double nextX = (x1 - x2) / (y1 - y2) * j + (y1 * x2 - y2 * x1) / (y1 - y2);
							result.push_back(Point(nextX, j));
						}
					}
					else
					{
						for (double j = y;j < nextY;j += 1)
						{
							double nextX = (x1 - x2) / (y1 - y2) * j + (y1 * x2 - y2 * x1) / (y1 - y2);
							result.push_back(Point(nextX, j));
						}
					}
				}
			}
			result.push_back(Point(right, (y1 - y2) / (x1 - x2) * right + (x1 * y2 - x2 * y1) / (x1 - x2)));
		}
		else
		{
			double buttom = min(A.y, B.y);
			double top = max(A.y, B.y);
			result.push_back(Point(A.x, buttom));
			for (double i = (int)buttom + 1;i < top;i += 1)
			{
				result.push_back(Point(A.x, i));
			}
			result.push_back(Point(A.x, top));
		}
		if (!endpoint)
		{
			result.erase(result.begin());
			result.erase(result.end());
		}
		return result;
	}
	static double getDistance(Point a, Point b)
	{
		return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
	}
	double getLineToLineDistance(Line num)
	{
		//根据两点式求出直线方程
		//(y2 - y1)x + (x1 - x2)y + x2y1 - x1y2 = 0
		//A=y2-y1     B=x1-x2      C=x2y1-x1y2
		if ((num.A.y - num.B.y) * (A.x - B.x) != (A.y - B.y) * (num.A.x - num.B.x))return 0;
		double A_equation = B.y - A.y;
		double B_equation = A.x - B.x;
		double C_1_equation = B.x * A.y - A.x * B.y;
		double C_2_equation = num.B.x * num.A.y - num.A.x * num.B.y;
		return abs(C_1_equation - C_2_equation) / sqrt(A_equation * A_equation + B_equation * B_equation);
	}
	double getLineLength()
	{
		return getDistance(A, B);
	}
	double getPointToLineDistance(Point p)
	{
		//根据|A*x0+B*y0+C|/sqrt(A^2+B^2)求点到直线的距离
		double AA = A.y - B.y;
		double BB = B.x - A.x;
		double CC = A.x * B.y - B.x * A.y;
		return abs(AA * p.x + BB * p.y + CC) / sqrt(AA * AA + BB * BB);
	}
	double getA_equation()
	{
		return B.y - A.y;
	}
	double getB_equation()
	{
		return A.x - B.x;
	}
	double getC_equation()
	{
		return B.x * A.y - A.x * B.y;
	}
	Point getIntersectionPoint(Line num)
	{
		//(y2 - y1)x + (x1 - x2)y + x2y1 - x1y2 = 0
		//x = (B2C1 - B1C2) / (A2B1 - A1B2)         y = (A1C2 - A2C1) / (A2B1 - A1B2)
		double A1 = (B.y - A.y);
		double B1 = (A.x - B.x);
		double C1 = B.x * A.y - A.x * B.y;

		double A2 = (num.B.y - num.A.y);
		double B2 = (num.A.x - num.B.x);
		double C2 = num.B.x * num.A.y - num.A.x * num.B.y;
		if (A2 * B1 == A1 * B2)return Point(-1, -1);
		return Point((B2 * C1 - B1 * C2) / (A2 * B1 - A1 * B2), (A1 * C2 - A2 * C1) / (A2 * B1 - A1 * B2));
	}
	Point getIntersectionPoint(Point outSide, double A_equation, double B_equation)
	{
		if (A_equation * this->getB_equation() == B_equation * this->getA_equation())return Point(0, 0);
		double C_equation = -A_equation * outSide.x - B_equation * outSide.y;
		double x = (B_equation * this->getC_equation() - this->getB_equation() * C_equation) /
			(A_equation * this->getB_equation() - this->getA_equation() * B_equation);
		double y = (this->getA_equation() * C_equation - A_equation * this->getC_equation()) /
			(A_equation * this->getB_equation() - this->getA_equation() * B_equation);
		return Point(x, y);
	}
	void setPointA(Point num) { A = num; }
	void setPointB(Point num) { B = num; }
	void operator=(Line num)
	{
		this->A = num.A;
		this->B = num.B;
	}
	void operator+(Point num)
	{
		this->A + num;
		this->B + num;
	}
};
